import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Main extends JFrame implements ActionListener {

	private static final long serialVersionUID = 3628156543984317078L;
	private Robot bot;
	private boolean running = false;

	public static void main(String[] args) {
		// start the
		new Main();
	}

	public void start() {

		Thread t = new Thread() {
			public void run() {
				while (true) {
					try {
						if (running) {
							int mask = InputEvent.BUTTON1_DOWN_MASK;
							bot.mousePress(mask);     
							bot.mouseRelease(mask);
						}
						Thread.sleep(75);
					} catch (InterruptedException e) {
					}
				}
			}
		};
		t.start();
	}

	public Main() {
		super();

		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		try {
			bot = new Robot();
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(0);
		}

		start();
		
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(200, 45));
		
		JButton start = new JButton("Start");
		start.addActionListener(this);
		start.setActionCommand("Start");
		panel.add(start);
		JButton stop = new JButton("Stop");
		stop.addActionListener(this);
		stop.setActionCommand("Stop");
		panel.add(stop);
		
		this.getContentPane().add(panel);

		this.pack();

		this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getActionCommand() == "Start") {
			running = true;
		} else {
			running = false;
		}
	}

}
